import BasicDataLoader from './basic-data-loader';
import env from '../../../env/env';
import ObjectUtils from '../../../utils/object-utils';
import AppError from '../../../exceptions/app-error';

declare let global: { fetch: {} };

describe('BasicDataLoader', () => {
    let service;

    const mockFetch = jest.fn();
    const originFetch = global.fetch;

    const URL = '/concrete/request';
    const envOrigin = ObjectUtils.cloneObject(env);

    beforeAll(() => {
        global.fetch = mockFetch;
    });

    beforeEach(() => {
        mockFetch.mockReturnValue(Promise.resolve({}));

        service = new BasicDataLoader({ baseApiUrlSuffix: '/baseApiSuffix' });
    });

    afterEach(() => {
        Object.assign(env, ObjectUtils.cloneObject(envOrigin));
    });

    afterAll(() => {
        global.fetch = originFetch;
    });

    describe('#get', () => {
        it('should use base api url and api suffix', () => {
            service.get(URL);
            expect(mockFetch.mock.calls[0][0]).toEqual('/data/baseApiSuffix/concrete/request');
        });

        it('should use parameter to enable cookies', () => {
            service.get(URL);

            const params = mockFetch.mock.calls[0][1];
            expect(params.credentials).toEqual('same-origin');
        });

        it('should return raw json on success', () => {
            const headersMap = new Map([['Content-Type', 'application/json; utf-8']]);

            mockFetch.mockReturnValue(Promise.resolve({
                ok: true,
                headers: headersMap,
                json: () => Promise.resolve({ id: 11 })
            }));

            return service.get(URL)
                .then(result => expect(result.id).toEqual(11))
                .catch(error => expect(error).toBeUndefined());
        });

        it('should throw app error on fail during request', () => {
            mockFetch.mockReturnValue(Promise.reject({
                message: 'Failed to fetch',
                stack: 'TypeError: Failed to fetch'
            }));

            return service.get(URL)
                .then(result => expect(result).toBeUndefined())
                .catch(error => {
                    expect(error.code).toEqual(0);
                    expect(error.message).toEqual('Failed to fetch');
                });
        });

        it('should throw app error on response with incorrect content type', () => {
            const headersMap = new Map([['Content-Type', 'text/html']]);

            mockFetch.mockReturnValue(Promise.resolve({
                ok: true,
                headers: headersMap,
                json: () => Promise.resolve({ id: 11 })
            }));

            return service.get(URL)
                .then(result => expect(result).toBeUndefined())
                .catch(error => {
                    expect(error.code).toEqual(0);
                    expect(error.message).toEqual('Response has incorrect content type');
                });
        });

        it('should throw app error on response without content type', () => {
            mockFetch.mockReturnValue(Promise.resolve({
                ok: true,
                headers: new Map(),
                json: () => Promise.resolve({ id: 11 })
            }));

            return service.get(URL)
                .then(result => expect(result).toBeUndefined())
                .catch(error => {
                    expect(error.code).toEqual(0);
                    expect(error.message).toEqual('Response has incorrect content type');
                });
        });

        it('should throw app error on response with fail flag', () => {
            const headersMap = new Map([['Content-Type', 'application/json; utf-8']]);

            mockFetch.mockReturnValue(Promise.resolve({
                ok: false,
                headers: headersMap,
                json: () => Promise.resolve({
                    code: 901401,
                    message: 'INCORRECT_LOGIN_OR_PASSWORD'
                })
            }));

            return service.get(URL)
                .then(result => expect(result).toBeUndefined())
                .catch(error => {
                    expect(error.code).toEqual(901401);
                    expect(error.message).toEqual('INCORRECT_LOGIN_OR_PASSWORD');
                });
        });

        it('should mock http status if option is set and raw data contains special field', () => {
            const headersMap = new Map([['Content-Type', 'application/json; utf-8']]);

            mockFetch.mockReturnValue(Promise.resolve({
                ok: true,
                headers: headersMap,
                json: () => Promise.resolve({
                    httpStatus: 401,
                    code: 102401,
                    message: 'Payment was failed.'
                })
            }));

            return service.get(URL)
                .then(result => expect(result).toBeUndefined())
                .catch(error => {
                    expect(error.code).toEqual(102401);
                    expect(error.message).toEqual('Payment was failed.');
                });
        });

        it('should not mock http status if option is not set', () => {
            const headersMap = new Map([['Content-Type', 'application/json; utf-8']]);

            mockFetch.mockReturnValue(Promise.resolve({
                ok: true,
                headers: headersMap,
                json: () => Promise.resolve({
                    httpStatus: 401,
                    code: 102401,
                    message: 'Payment was failed.'
                })
            }));

            env.mockHttpStatusHandler = false;

            return service.get(URL)
                .then(result => {
                    expect(result instanceof AppError).toBeFalsy();
                    expect(result.code).toEqual(102401);
                })
                .catch(error => expect(error).toBeUndefined());
        });
    });

    describe('#post', () => {
        it('should add special headers and serialize payload to send json content', () => {
            env.mockRequestMethod = false;

            service.post(URL, { id: 1, name: 'test' });

            const actualUrl = mockFetch.mock.calls[0][0];
            const actualRequestParams = mockFetch.mock.calls[0][1];

            expect(actualUrl).toEqual('/data/baseApiSuffix/concrete/request');
            expect(actualRequestParams.method).toEqual('POST');
            expect(actualRequestParams.headers).toEqual({
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            });
            expect(actualRequestParams.body).toEqual('{"id":1,"name":"test"}');
        });

        it('should add additional debug header and exclude body if flag to mock request method is specified', () => {
            env.mockRequestMethod = true;

            service.post(URL, { id: 1, name: 'test' });

            const actualRequestParams = mockFetch.mock.calls[0][1];

            expect(actualRequestParams.method).toEqual('GET');
            expect(actualRequestParams.headers['X-Dev-Body']).toEqual('{"id":1,"name":"test"}');
            expect(actualRequestParams.body).toBeUndefined();
        });

        it('should correctly send request without content', () => {
            env.mockRequestMethod = false;

            service.post(URL);

            const actualUrl = mockFetch.mock.calls[0][0];
            const actualRequestParams = mockFetch.mock.calls[0][1];

            expect(actualUrl).toEqual('/data/baseApiSuffix/concrete/request');
            expect(actualRequestParams.method).toEqual('POST');
            expect(actualRequestParams.headers).toEqual({
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            });
            expect(actualRequestParams.body).toBeUndefined();
        });
    });
});