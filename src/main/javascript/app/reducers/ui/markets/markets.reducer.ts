import { UiActionType } from '../../../actions/ui/ui-action-type';
import CommonAction from '../../../actions/common/types/common-action';
import MarketUiState from '../../../entities/ui/market-ui-state/market-ui-state';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';
import LeagueEntities from '../../../services/data-loaders/league-data-loader/league-entities';

const initialState = new Map<number, MarketUiState>();

export default function marketsReducer(state: Map<number, MarketUiState> = initialState, action: CommonAction) {
    switch (action.type) {
        case DomainModelActionType.FETCH_LEAGUES_SUCCESS: {
            const leagueEntities = action.payload as LeagueEntities;
            const periods = leagueEntities.periods;
            const resultState = new Map<number, MarketUiState>(state);

            for (const period of Array.from(periods.values())) {
                prepareMarketGroup(period.handicap, resultState);
                prepareMarketGroup(period.totals, resultState);
                prepareMarketGroup(period.teamHomeTotals, resultState);
                prepareMarketGroup(period.teamAwayTotals, resultState);
            }

            return resultState;
        }
        case UiActionType.SELECT_MARKET: {
            const marketId = action.payload.marketId as number;
            const marketSiblingIds = action.payload.marketSiblingIds as number[];
            const resultState = new Map<number, MarketUiState>(state);

            marketSiblingIds.forEach(id => {
                const marketUiState = new MarketUiState(resultState.get(id));
                marketUiState.selected = id === marketId;
                resultState.set(id, marketUiState);
            });

            return resultState;
        }
        default:
            return state;
    }
}

function prepareMarketGroup(marketIds: number[], resultState: Map<number, MarketUiState>) {
    let isGroupAlreadyLoaded = false;
    const marketList: MarketUiState[] = [];

    marketIds.forEach(marketId => {
        if (!resultState.has(marketId)) {
            const market = new MarketUiState();
            resultState.set(marketId, market);
            marketList.push(market);
            return;
        }

        // Mark group as already loaded
        // if someone market has been already loaded
        isGroupAlreadyLoaded = true;
    });

    if (!isGroupAlreadyLoaded) {
        selectMiddleItem(marketList);
    }
}

function selectMiddleItem(list: any[]) {
    if (list.length === 0) {
        return;
    }
    const middleIndex = Math.floor(list.length / 2);
    list[middleIndex].selected = true;
}