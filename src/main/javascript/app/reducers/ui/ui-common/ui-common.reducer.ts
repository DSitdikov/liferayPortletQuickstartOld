import { combineReducers } from 'redux';
import busyReducer from './busy/busy.reducer';
import errorReducer from './error/error.reducer';
import notificationReducer from './notification/notification.reducer';

const uiCommonReducer = combineReducers({
    busy: busyReducer,
    error: errorReducer,
    notification: notificationReducer
});

export default uiCommonReducer;