import CommonAction from '../../../../actions/common/types/common-action';
import AppError from '../../../../exceptions/app-error';
import { UiActionType } from '../../../../actions/ui/ui-action-type';

export default function errorReducer(state: AppError | null = null, action: CommonAction) {
    if (action.type === UiActionType.SHOW_ERROR) {
        return action.payload as AppError;
    }
    return state;
}