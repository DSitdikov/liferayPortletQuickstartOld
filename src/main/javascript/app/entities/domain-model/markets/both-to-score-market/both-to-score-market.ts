import SimpleLineMarket from '../simple-line-market/simple-line-market';
import { MarketType } from '../common/market-type';

export default class BothToScoreMarket extends SimpleLineMarket {

    constructor(data: any = {}) {
        super({
            ...data,
            type: MarketType.BOTH_TO_SCORE
        });
    }

    selectOutcomeName() {
        return '';
    }
}
