import { OutcomeType } from '../../outcome/outcome-type';
import LeagueEvent from '../../league-event/league-event';
import MoneyLineMarket from './money-line-market';

describe('MoneyLineMarket', () => {
    it('should create default instance', () => {
        const market = new MoneyLineMarket();

        expect(market.id).toEqual(0);
        expect(market.originId).toEqual(0);
        expect(market.periodId).toEqual(0);
        expect(market.type).toEqual('ML');
        expect(market.outcomeB.formatted).toEqual('No line');
        expect(market.outcomeA.formatted).toEqual('No line');
        expect(market.draw.formatted).toEqual('No line');
    });

    it('should return outcome name', () => {
        const event = new LeagueEvent({
            home: 'Chelsea',
            away: 'Ural'
        });
        const market = new MoneyLineMarket({
            id: 47370,
            periodId: 47551,
            away: 1.40,
            home: 3.01,
            hdp: 0.6
        });

        expect(market.getOutcomeName(OutcomeType.AWAY, event)).toEqual('Ural ML');
        expect(market.getOutcomeName(OutcomeType.HOME, event)).toEqual('Chelsea ML');
        expect(market.getOutcomeName(OutcomeType.DRAW, event)).toEqual('Draw');
    });

    it('should return default outcome name if outcome type is undefined or wrong', () => {
        const market = new MoneyLineMarket();
        const event = new LeagueEvent();

        expect(market.getOutcomeName('random value', event)).toEqual('');
        expect(market.getOutcomeName('', event)).toEqual('');
    });
});