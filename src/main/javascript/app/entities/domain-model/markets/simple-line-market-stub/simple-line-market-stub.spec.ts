import SimpleLineMarketStub from './simple-line-market-stub';

/**
 * Used only for testing
 */
describe('SimpleLineMarketStub', () => {
    it('should create default instance', () => {
        const market = new SimpleLineMarketStub();

        expect(market.id).toEqual(0);
        expect(market.originId).toEqual(0);
        expect(market.periodId).toEqual(0);
        expect(market.type).toEqual('Unknown');
        expect(market.outcomeB.formatted).toEqual('No line');
        expect(market.outcomeA.formatted).toEqual('No line');
        expect(market.draw.formatted).toEqual('No line');
    });

    it('should parse raw market', () => {
        const market = new SimpleLineMarketStub({
            id: 47544,
            periodId: 33121,
            type: 'ML',
            away: 1.3,
            home: 10.518,
            draw: 0
        });

        expect(market.id).toEqual(47544);
        expect(market.originId).toEqual(47544);
        expect(market.periodId).toEqual(33121);
        expect(market.type).toEqual('ML');
        expect(market.outcomeB.formatted).toEqual('1.30');
        expect(market.outcomeA.formatted).toEqual('10.52');
        expect(market.draw.formatted).toEqual('No line');
    });
});
