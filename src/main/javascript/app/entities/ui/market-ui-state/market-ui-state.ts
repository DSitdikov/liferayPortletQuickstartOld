export default class MarketUiState {
    selected: boolean;

    constructor(data: any = {}) {
        this.selected = Boolean(data.selected);
    }
}