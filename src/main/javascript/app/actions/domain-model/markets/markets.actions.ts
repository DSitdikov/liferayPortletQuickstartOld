import { DomainModelActionType } from '../domain-model-action-type';
import { ActionCreator } from 'redux';
import Market from '../../../entities/domain-model/markets/common/market';
import CommonAction from '../../common/types/common-action';

export const updateMarket: ActionCreator<CommonAction> = (market: Market) => ({
    type: DomainModelActionType.UPDATE_MARKET,
    payload: market
});
