import { ActionCreator, Dispatch } from 'redux';
import { UiActionType } from '../ui-action-type';
import CommonAction from '../../common/types/common-action';
import { ThunkAction } from 'redux-thunk';
import StoreState from '../../../entities/store-state';
import League from '../../../entities/domain-model/league/league';
import LeagueEvent from '../../../entities/domain-model/league-event/league-event';
import EventUiState from '../../../entities/ui/event-ui-state/event-ui-state';
import LeagueUiState from '../../../entities/ui/league-ui-state/league-ui-state';

export const toggleSelectionOfLeague: ActionCreator<ThunkAction<CommonAction, StoreState, void>> = (id: number) => {
    return (dispatch: Dispatch<StoreState>, getState: () => StoreState): CommonAction => {
        const state = getState();
        const leagues = state.entities.leagues;
        const eventIds = (leagues.get(id) as League).events;

        return dispatch({
            type: UiActionType.TOGGLE_SELECTION_OF_LEAGUE,
            payload: {
                leagueId: id,
                eventIds
            }
        });
    };
};

export const toggleExpandStateOfLeague: ActionCreator<ThunkAction<CommonAction, StoreState, void>> = (id: number) => {
    return (dispatch: Dispatch<StoreState>, getState: () => StoreState): CommonAction => {
        const state = getState();
        const leagueUiStates = state.ui.leagues;

        let sportExpanded = true;
        for (const leagueId of Array.from(leagueUiStates.keys())) {
            const leagueUiState = leagueUiStates.get(leagueId) as LeagueUiState;
            sportExpanded = sportExpanded && (leagueId !== id ? leagueUiState.expanded : !leagueUiState.expanded);
        }

        return dispatch({
            type: UiActionType.TOGGLE_EXPAND_STATE_OF_LEAGUE,
            payload: {
                leagueId: id,
                sportExpanded
            }
        });
    };
};

export const toggleSelectionOfEvent: ActionCreator<ThunkAction<CommonAction, StoreState, void>> = (id: number) => {
    return (dispatch: Dispatch<StoreState>, getState: () => StoreState): CommonAction => {
        const state = getState();
        const event = state.entities.events.get(id) as LeagueEvent;
        const eventUiStates = state.ui.events;
        const league = state.entities.leagues.get(event.leagueId) as League;

        let leagueSelected = true;
        league.events.forEach(eventId => {
            const eventUiState = eventUiStates.get(eventId) as EventUiState;
            leagueSelected = leagueSelected && (eventId !== id ? eventUiState.selected : !eventUiState.selected);
        });

        return dispatch({
            type: UiActionType.TOGGLE_SELECTION_OF_EVENT,
            payload: {
                eventId: id,
                leagueId: event.leagueId,
                leagueSelected
            }
        });
    };
};