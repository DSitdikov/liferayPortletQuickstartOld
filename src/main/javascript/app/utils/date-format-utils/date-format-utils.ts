export default class DateFormatUtils {

    static formatDateDifference(differenceInMillis: number) {
        const minuteMask = 60;
        const hourMask = 60 * minuteMask;
        const dayMask = 24 * hourMask;

        let differenceRest = Math.floor(differenceInMillis / 1000);

        const daysCount = Math.floor(differenceRest / dayMask);
        differenceRest = differenceRest - daysCount * dayMask;
        const hoursCount = Math.floor(differenceRest / hourMask);
        differenceRest = differenceRest - hoursCount * hourMask;
        const minutesCount = Math.floor(differenceRest / minuteMask);

        return [
            daysCount ? `${daysCount}d ` : '',
            hoursCount ? `${hoursCount}h ` : '',
            `${minutesCount}m`
        ].join('');
    }
}